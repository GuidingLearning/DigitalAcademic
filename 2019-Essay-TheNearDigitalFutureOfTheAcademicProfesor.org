#+TITLE: The Near Digital Future of the Academic Profesor
#+DESCRIPTION: An essay describing the effect of new digital tecnologies on the work of an academic profesor.
#+AUTHOR: Jérémy Barbay 
# #+EMAIL: jeremy@barbay.cl
#+CATEGORY: Ensayos
#+OPTIONS: toc:nil
#+LATEX_CLASS: article
#+LATEX_HEADER_EXTRA: \usepackage{fullpage}
#+LANGUAGE: es
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport

#+begin_abstract  
Supposedly, the academic profesor is at the edge of research and of digitalisation: Arpanet was born to connect universities, and the first electronic emails were sent between academic profesors. But in practice, many aspects of the academic work have only changed in superficial ways in the last 50 years: in the time of a scientist such as Einstein, one sent articles by "snail" mail and now it is sent by electronic mail or by submitting it to a website, but the information comunicated, and its format, are essentially the same. 

We describe various aspects (education, research and administration) of the digital revolution yet to come in the context of the career of an academic profesor, based as much in the lower cost and potential for masification of the technologies of digital processing of information, as in the techniques which impact such work in more indirect ways, such as protocols of collaborative quality control (e.g. ReCaptcha, Calibrated Peer Review), encryption (e.g. asymetric keys, electronic signatures) and personalisation (e.g. big data), leaving aside techniques which impact seem less specific to the academic career (e.g. Machine Learning and Artificial Intelligence).
#+end_abstract 

* Introduction

Las responsabilidades de un profesor universitario pueden categorizarse, a grandes rasgos, en tres componentes principales: /investigación/, /docencia/, y /administración/. La realización de tales componentes requiere de la generación y/o transmisión de información entre distintas personas. A medida que nuestra sociedad avanza, los medios para realizar tales traspasos han evolucionado considerablemente. Sin embargo, las forma de interactuar con estos medios siguen sin cambiar mucho. Seria posible apuntar a algunos cambios, deseables o predecibles? Describamos algunos elementos de respuesta en cada uno de las tres componentes de las responsabilidades de un profesor universitario.
	
# Research

# Teaching

# Administration

# General Bridge to other sections

** NOTES                                                           :noexport:
 La aparición de las computadoras, la capacidad de almacenar cada vez más grandes cantidades de datos y la internet como medio de trasmisión rápido y menos costoso, ha hecho que el acceso a conocimiento y la conectividad entre aquellos que lo posean sea cada vez más fácil, rápido y democrático.

	 Algunos ejemplos de esto son [NOTA VANE: reescribir más mejor]:
	 - Proyectos que permiten que cada alumno tenga un libro [NOTA VANE: Docencia alumnos tengas acceso a conocimiento]
	 - MOOCS como puente entre alumnos y profesores [NOTA VANE: docencia, relación entre alumnos y profesores]
	 - Journals digitales/conferencias + proyectos como sci-hub. [NOTA VANE: investigación] [NOTA VANE: podemos hablar de sci-hub o es muy ilegal?] [NOTA JYBY: Quiero hablar de Sci-Hub, y de la logica atras]

	 Sin embargo, aunque avanzamos rápido en términos de almacenamiento y acceso de información, podemos observar que el formato en el que está compactado/displayed esta información no ha variado mucho con los años. [NOTA VANE: poner ejemplos].
		
	 Finalmente, existe una tercera tarea que debe considerar un profesor universitario y que relaciona tanto la investigación como la docencia: el trabajo admnistrativo. Esta tarea sufre de problemas parecidos que las otras dos anteriormente descritas [NOTA VANE: comprobar]. Existe mucho avance en términos de cómo almacenar, acceder y desplegar estos datos pero muchas veces los procesos para procesarlos son los mismos. [NOTA VANE: acá necesito confirmación de Jérémy porque no conozco mucho de esto.]

	 En este ensayo....

	 #+
	 Abajo dejé toda la estructura original para no perderla.
	 #+

	1) [X] evolucion del material de transmision de los conocimientos
	   - tradicion oral
	   - papel
	   - papel imprimido
	   - digital
	2) [ ] etapas de una revolucion
	   - inovaciones tecnologicas se acumulan
	   - una ultime inovacion sirve de "trigger" y
	   - el campo se revoluciona
	   - Ejemplo: 
	     - el perfecionamiento de la prensa por Gutemberg en la cultura occidental,
	       - por mucho tiempo se aplico solamente a imprimir las mismas biblias que copian los monjes (una revolucion menor en simismo),
	       - pero revoluciono
		 - la politica cuando permito de imprimir pamphlets para distribuir,
		 - la docencia con la impresion de libros 
	3) [ ] la revolucion digital en docencia a pena a iniciado
	   - todavia se generan libros electronicos y apuntes onlines como se generan libros (un poco como los primeros libros imprimidos eran biblias)
	   - a pena se inicia de generalizar el concepto de fuentes interactiva de conocimientos (e.g. Jupyter notebooks, serious games, MOOCS, etc...), todavia muy basados en sus predecesores (e.g. MOOCS son clases y exercicios cortos con AB Testing, pero todavia son clases y exercicios)
	   - En este ensayo, pretendo describir y analizar algunos aspectos predecibles de la revolucion digital a venir en la profesion de profesor academico, tan del punto de vista de la docencia, que de la investigacion y de la administracion.
* Research

** NOTES :noexport:
      1) Automatizacion del proceso de bibliografia
	 - e.g. Google Warnings cuando un articulo esta publicado que refiere a lo suyo o usa terminos de interes para su investigacion.
      2) Reproducible research
	 - Articles escritos como Jupyter notebooks, cuales permitten no solamente de maximizar la reproducibilidad de los experimentos y del analisis de sus resultados, pero tambien de facilitar la exploracion de variantas de tales experimentos y analisis.
      3) Quien revisa a los revisores?
	 - Sitio(s) web en cual(es) los investigadores jovenes, al mismo tiempo revisan articulos de sus pares, y aprenden a revisar (y a escribir) tales articulos.
** Comunication and Reproducibility
** Peer-Review Validation
** Anonimity

The evaluation of academic work, whether scientific or educational, is prone to gender and racial bias: students evaluate their professor based on distinct critters depending of their gender, scientists evaluate scientific reports distinctly according to the gender and culture suggested by the name of the authors. Experiments comparing evaluations of scientific work showed a reduction of such biases when anonymizing the authors, and one can expect a similar bias in the citation rate of non-anonymous articles. A system of signed certificates (such as described in the section Administración) would permit completely anonymized evaluation and publication of scientific reports, letting scientists decide to make their authorship public or not (with the tools to certify such authorship), solving the current gender and racial issue.

* Teaching

** NOTES :noexport:
       1) (further) amortizacion del costo de produccion de material docente
	  - produccion de material pedagogico advanzado (teleserias educativas en vez de clases suggeridas por Luis von Ahn, juegos videos pedagogicos / serious games)
	  - base de datos colectivas (decentralisadas) de material docente (http://repositorium.cl)
       2) A/B testing de tecnicas de docencia en linea 
	  - MOOCS tales que coursera, futurelearn, etc...
	  - estadisticas detalladas sobre cursos masivos (sobre varios años)
       3) Personalizacion de la docencia
	  - Student Profiling (mas aya de los ipoteticos "learner types")
	  - spaced repetition (mas aya de lesiones de vocabulario)
** Collaboration
** Research in Education
** Personalized Education
* Administration

As the number and sizes of universities grow, so does the administrative load associated to their management, as much for "administrators" as for "academics".  The aspects of the job of an academic profesor (and the time dedicated to those) vary from one institution to another, but those are invariably the ones which potential for automatization has been the most ignored. We describe here how some well established technologies on database management and digital signatures would permit to /automatize/ the comunication between administrators and academics (hence reducing the workload of both) in a distributed and secure way; and how such automatization will permit, through its statistical analysis, to measure and /compare the impact of incentives/ and other policies across institutions, and in particular to /detect counter-productive incentives/, whether for errors in their design, corruption or lack of scalability.

** Reporting

Bottom-up reporting is one of the most costly requirement to manage a large hierarchical institution, and can be automatized, much more than they currently are. It is all the more costly in the case of academic institutions because they have multiple overlapping objectives, such as research and education (but also advising on public policies, popularisation, etc.). In the paper era and smaller institution, such reporting was either not done or done in a limited way through paper forms. The rise of digital technologies so far only replaced paper forms by digital ones, while the administrative growth multiplied those, resulting in many redundant digital ones. The revolution in this respect will happen at the political level (with no new technological development required), when a universally agreed standard will allow to automatically fill such forms from a unique database filled by the academics, resulting in more information for the administrators at less cost for the academics.

** Checking
   
The cost of accountability and verification is already a problem, and will become much worse both as academia grows (in volume and potentially in competitivity) and as digital standards allow more, better and cheaper communication. Techniques which would allow a researcher to certify a publication in a prestigious journal on his CV, or a teacher to justify a prize received, are similar in essence to that which allow a software vendor to certify the origin of the software (as opposed to some malware), or for a DNS server to certify the answer to a DNS query (DNSSec). Certifying software solutions, using on well established cryptographic techniques such as pairs of public and private cryptographic keys (RSA), are already available and it would "only" be a political decision to use them for each institution (e.g. publishers) to issue digital certificates to the agents (e.g. researcher, educators), which can in turn be communicated to their employers (actual or potential) and checked in a completely automated way by software.

** Evaluating Policies

Beyond the day to day routine of reporting and checking, an efficient administration must be able to evaluate past and current situations in order to plan the future. The gathering of large volumes of trusted information digitally at academic institutions with distinct policies will permit to compare experimentally the impact of such policies, with a degree of detail never achieved so far.  With such tools, one will identify which incentives have effects contrary to their intent, or even corrupt behaviors taking advantage of gaps in the policies, and be able to remedy to such situations by realizing the appropriate changes to the rules.


* Conclusion

** NOTES :noexport:
   1) En este ensayo pretendimos describir y analizar algunos aspectos predecibles de la revolucion digital a venir en la profesion de profesor academico, tan del punto de vista de la docencia, que de la investigacion y de la administracion.
   2) El futuro queda a escribir, y quedan muchos otros temas "digitales" que prestan a revoluciones en la carrera de un profesor academico,
      - desde aplicaciones de "machine learning" al diseño de asistantes digitales para profesores academicos,
      - hasta el potencial de soluciones de tele-presencia sobre las collaboraciones y interacciones de profesores academicos en un mundo donde los viajes estaran mas dificiles.
   3) Non-obstante, sentimos que la transicion digital a venir en la carrera de profesores academicos, que ha sido lenta hasta el momento, podria ponerse muy rapida subitamente, en conjunction con cambios predecidos en otros aspetos de la sociedad, por lo cual es necesario prepararse a una tal transicion la mas rapidamente posible.

** Summary of the Report
** Ignored themes
** Impact hoped for
* REFERENCES
** Guns, Germs, and Steel: The Fates of Human Societies, Jared Diamond 1999
#+BEGIN_QUOTE
Paperback: 480 pages
Publisher: W. W. Norton & Company; 1st edition (April 1, 1999)
Language: English
ISBN-10: 0393317552
ISBN-13: 978-0393317558
https://www.amazon.com/Guns-Germs-Steel-Fates-Societies/
#+END_QUOTE
** Iconoclast, Gregory Berns, 2010
#+BEGIN_QUOTE
Harvard Business Review Press
(March 17, 2010)
272 pages
ISBN: 978-1422133309
http://gregoryberns.com/iconoclast.html
#+END_QUOTE
